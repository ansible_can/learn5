# Learn3

This is just an ansible learning project. This part is the fourht part of the project and here is just an example of handlers variables.

You should have implemented the first part (bootstrap) before coming to this point.


## Getting started

All hosts are linux based are either CentOS or Ubuntu.
But there are prerequisites. The root should be able to login via ssh at the beginning. This should be done passwordless
so we have to send public key.
```
ssh-copy-id -i ~/.ssh/id_rsa <192.168.0.160 -> this is host IP>
```

#  To run 
```
ansible-playbook playbook.yml
```
